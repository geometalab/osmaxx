version: '3.7'

services:
  proxy:
    image: geometalab/env-configurable-caddy:latest
    environment:
      # See https://caddyserver.com/docs/quick-starts/caddyfile for details
      CADDY_CONFIG: |
        # replace with your domain-name and expose ports 443 and 80
        # if you are not running behind another reverse proxy
        # else remove everything before the ':' in the line below
        localhost {
          route {
            handle /media/* {
              uri strip_prefix /media
              file_server /* {
                root /data/media/
              }
            }
            handle /static/* {
              uri strip_prefix /static
              file_server /* {
                root /data/static/
              }
            }
            reverse_proxy /* {
              to frontend:8000
            }
          }
        }
    volumes:
      - frontend-static:/data/static
      - frontend-media:/data/media
      - worker-data:/data/media/job_result_files
    ports:
    # replace with correct ports
    - 0.0.0.0:8443:443/tcp
    - 0.0.0.0:8080:80/tcp
    networks:
      - default
    logging: &logging
      driver: json-file
      options:
        max-size: 50m

  ##### frontend START ########
  frontend: &frontend
    image: geometalab/osmaxx-frontend:latest
    volumes:
      - frontend-media:/data/media
      - frontend-static:/data/static
      - worker-data:/data/media/job_result_files
      - osm_data:/var/data/osm-planet
    environment: &baseenv
      # set this to your domain name!
      ? DJANGO_ALLOWED_HOSTS=localhost
      ? DJANGO_CSRF_TRUSTED_ORIGINS=https://localhost
      ? DJANGO_SECRET_KEY=insecure!1
      ? DJANGO_SETTINGS_MODULE=config.settings
      # To enable login through OSM:
      #     copy .env-dist to .env and fill the SOCIAL_AUTH_OPENSTREETMAP_KEY and SOCIAL_AUTH_OPENSTREETMAP_SECRET

      # Allow access from any private-use IP, since docker assigns "random" IPs.
      #     172.*.*.* is actually allowing too much, but this docker-compose file should
      #     only ever be used on local development machine, anyway!
      ? DJANGO_INTERNAL_IPS=172.*.*.*,10.*.*.*,192.168.*.*,127.0.0.1
      ? DJANGO_EMAIL_URL=consolemail://
      ? DJANGO_DEFAULT_FROM_EMAIL=webmaster@osmaxx.exmaple.com
      ? DJANGO_SERVER_EMAIL=webmaster@osmaxx.exmaple.com
      ? OSMAXX_ACCOUNT_MANAGER_EMAIL=webmaster@osmaxx.exmaple.com
      ? DJANGO_LOG_LEVEL=INFO
      # set these to true only if you have a secure proxy in front of the frontend!
      ? DJANGO_CSRF_COOKIE_SECURE=false
      ? DJANGO_SESSION_COOKIE_SECURE=false
      ? DJANGO_DATABASE_URL=postgis://frontend:insecureChangeInProduction!@frontenddatabase/frontend
      ? CELERY_BROKER_URL=redis://redis:6379/1
    env_file:
      - .env
    networks:
      - default
    restart: on-failure
    logging:
      <<: *logging

  setup:
    <<: *frontend
    image: geometalab/osmaxx-setup:latest

  frontend-purge-files:
    <<: *frontend
    image: geometalab/osmaxx-file-purge:latest

  frontenddatabase:
    image: postgis/postgis:15-3.3
    volumes:
      - frontend-database-data:/database/data
    environment:
      - PGDATA=/database/data
      - POSTGRES_DB=frontend
      - POSTGRES_USER=frontend
      - POSTGRES_PASSWORD=insecureChangeInProduction!
    networks:
      - default
    logging:
      <<: *logging

  redis:
    image: redis
    networks:
      - default
    logging:
      <<: *logging

  worker: &worker
    <<: *frontend
    image: geometalab/osmaxx-worker:latest
    environment:
      <<: *baseenv
      ? DJANGO_SECRET_KEY=insecure!3
      ? GIS_CONVERSION_DB_HOST=worker-db
      # needs to be the password of the conversion db!
      ? PGPASSWORD=worker
      ? GIS_CONVERSION_DB_PASSWORD=worker
      ? GIS_CONVERSION_DB_USER=worker
      ? OSM_BOUNDARIES_DB_NAME=osmboundaries 
      # set this at maximum your availbale number of cores
      ? CELERY_WORKER_CONCURRENCY=4
    networks:
      - default
    logging:
      <<: *logging

  scheduler:
    <<: *worker
    image: geometalab/osmaxx-scheduler:latest

  worker-db:
    image: geometalab/osmaxx-worker-db:latest
    # this can and probably should be further optimized
    # see https://www.postgresql.org/docs/current/app-postgres.html
    command: postgres --shared_buffers=2GB --synchronous_commit=off --full_page_writes=off --fsync=off --max_wal_size=10GB --autovacuum=off --checkpoint_timeout=360
    volumes:
      - worker-db:/database
    environment:
      - PGDATA=/database/data
      - POSTGRES_DB=worker
      - POSTGRES_USER=worker
      - POSTGRES_PASSWORD=worker
    logging:
      <<: *logging

  osmboundaries-importer:
    # to build/update this image see `docker-helper-images/osmboundaries/`
    image: geometalab/osmboundaries:latest
    environment:
      # TODO: make osmboundaries more flexible concerning the database name
      # db-name must be osmboundaries. this is a hard-coded dependency
      - POSTGRES_DB=osmboundaries
      # the same as the ones in the database above
      - POSTGRES_USER=worker
      - POSTGRES_PASSWORD=worker
      - POSTGRES_HOST=worker-db
    networks:
      - default
    logging:
      <<: *logging

  ##### CONVERSION SERVICE END ########
  ##### PBF UPDATER START ########
  # change to include the world accordingly!
  osm-pbf-updater:
    image: geometalab/osm-pbf-updater:latest
    volumes:
      - osm_data:/var/data/osm-planet
    # use this setting if not the entire world (osm-pdf) should be used!
    # environment:
    #   - osmupdate_extra_params=--base-url=download.geofabrik.de/europe/switzerland-updates/
    #   - osm_planet_mirror=http://download.geofabrik.de/
    #   - osm_planet_path_relative_to_mirror=europe/switzerland-latest.osm.pbf
    networks:
      - default
    logging:
      <<: *logging

  ##### PBF UPDATER END ########
volumes:
  # osmaxx
  frontend-database-data: {}
  frontend-media: {}
  frontend-static: {}
  worker-data: {}
  worker-db: {}
  osm_data: {}
