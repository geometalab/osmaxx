#!/usr/bin/env python
from collections import namedtuple
import subprocess


IMAGES = [
    dict(
        image_name="geometalab/osmaxx-frontend",
        context="src/",
        dockerfile="docker/Dockerfile",
        target="frontend",
    ),
    dict(
        image_name="geometalab/osmaxx-worker",
        context="src/",
        dockerfile="docker/Dockerfile",
        target="worker",
    ),
    dict(
        image_name="geometalab/osmaxx-scheduler",
        context="src/",
        dockerfile="docker/Dockerfile",
        target="scheduler",
    ),
    dict(
        image_name="geometalab/osmaxx-setup",
        context="src/",
        dockerfile="docker/Dockerfile",
        target="setup",
    ),
    dict(
        image_name="geometalab/osmaxx-file-purge",
        context="src/",
        dockerfile="docker/Dockerfile",
        target="file-purge",
    ),
    dict(
        image_name="geometalab/osmaxx-worker-db",
        context="docker-helper-images/osmaxx-postgis-translit/",
        dockerfile="docker-helper-images/osmaxx-postgis-translit/Dockerfile",
    ),
    dict(
        image_name="geometalab/osm-pbf-updater",
        context="docker-helper-images/osm_pbf_updater/",
        dockerfile="docker-helper-images/osm_pbf_updater/Dockerfile",
    ),
    # osmboundaries needs special files,
    # to udpate please follow the instructions there
    # (especially regarding the update_shapfiles.sh!)
]


def docker_build(dockerfile, image_name, release, context='.', target=None):
    command = [
        "docker",
        "build",
        "--pull",
        "-f",
        f"{dockerfile}",
    ]
    if target:
        command.extend(["--target", target])
    command.extend(
        [
            "-t",
            "{}:{}".format(image_name, release),
            context,
        ]
    )
    subprocess.check_call(command)


def docker_push(release, image_name, *args, **kwargs):
    subprocess.check_call(["docker", "push", "{}:{}".format(image_name, release)])


def parse_args():
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument("-t", "--tag", help="image tag (default is dev plus git hash, if not release tag and on master)")
    parser.add_argument("--ignore-dirty", help="ignore dirty git status (default is to append hash if dirty)", action=argparse.BooleanOptionalAction)
    parser.add_argument("--no-push", help="don't push images, only build them", action=argparse.BooleanOptionalAction)

    args = parser.parse_args()
    print(args)
    return namedtuple('Params', 'tag ignore_dirty push')(args.tag, args.ignore_dirty, args.no_push is not False)

if __name__ == "__main__":
    params = parse_args()

    is_dirty = subprocess.check_output(["git", "diff", "--stat"]).strip().decode()
    git_version = subprocess.check_output(["git", "describe", "--tags", "--dirty"]).strip().decode()
    release = subprocess.check_output(["poetry", "version"], cwd='src/').strip().decode().replace('osmaxx ', '')

    print()
    print(f'Version Info')
    print(f'git version (latest tag): {git_version}')
    print(f'release version (pyproject.toml): {release}')
    print('**release has precedence over tag for image tag name**')
    
    tag = params.tag
    if git_version != release and not tag:
        tag = f'{release}-dev'
    if is_dirty and not params.ignore_dirty:
        tag = f'{release}-dirty'
    if not tag:
        tag = release

    print()
    print(f'using "{tag}" as tag')
    if not params.push:
        print("not pushing images, only building them")
    print()
    
    print("use any letter except y or Y to stop (or just hit enter to abort)")
    should_continue = input("Is this correct? (y): ")
    while should_continue not in ("Y","y"):
        exit(0)
    
    for image in IMAGES:
        docker_build(release=tag, **image)
    if params.push:
        for image in IMAGES:
            docker_push(release=tag, **image)

    # release versions that do not contain breaking changes
    if release == tag and git_version == release:
        shorter_release = release[:-2]
        for image in IMAGES:
            docker_build(release=shorter_release, **image)
        if params.push:
            for image in IMAGES:
                docker_push(release=shorter_release, **image)
        shorter_release = shorter_release[:-2]
        for image in IMAGES:
            docker_build(release=shorter_release, **image)
        if params.push:
            for image in IMAGES:
                docker_push(release=shorter_release, **image)
        # release latest images as well
        for image in IMAGES:
            docker_build(release="latest", **image)
        if params.push:
            for image in IMAGES:
                docker_push(release="latest", **image)
    else:
        print(f"not releasing any 'sub-versions', since release {release} is not the same as git version {git_version} and tag {tag} differs as well.")

    print(f"{tag} for the release {release} has been pushed, you can now use that in your deployment!")
