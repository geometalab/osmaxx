from django.apps import AppConfig


class ClippingAreaConfig(AppConfig):
    name = 'osmaxx.clipping_area'
    verbose_name = "OSMaxx Clipping Area"
