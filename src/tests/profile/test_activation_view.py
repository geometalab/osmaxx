import pytest

from django.urls import reverse



VERIFICATION_FAILED_MESSAGE = "Verification token too old or invalid. Please resend the confirmation email and try again."
VERIFICATION_SUCCESS_MESSAGE = "Successfully verified your email address."
