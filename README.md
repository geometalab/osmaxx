# OSMaxx

OSM arbitrary e**x**cerpt e**x**traction.

## Folder description

```bash
├── docker
├── docker-compose.yml
├── docker-helper-images
├── docs
├── LICENSE
├── MANIFEST.in
├── mkdocs.yml
├── src
├── src/tests
└── README.md
```

* `docker`: docker specific setup. Currently only the dockerfile.
* `docker-compose.yml`: the (local) setup description to run docker containers in development
* `docker-helper-images`: All used docker images, that are used in this project, but are a bt othogonal
* `docs`: documentation
* `LICENSE`: MIT Licence for OSMaxx
* `MANIFEST.in`: Python description file (https://packaging.python.org/en/latest/guides/using-manifest-in/)
* `mkdocs.yml`: Documentation generator (https://www.mkdocs.org/)
* `src`: The source files of the osmaxx project
* `src/tests`: test for osmaxx
* `README.md`: The Readme, "standard" starting point for a OS project

## Production Deployment

FIXME: Describe the steps needed for production deployment

To deploy OSMaxx, you need to have a working docker installation.

The follwoing description assumes a deployment on a linux server,
if you should use someting like kubernetes or portainer, you should be able
to get "inspiration" from the docker-compose.prod.yml.

1. Download the latest docker-compose.prod.yml  and save it as docker-compose.yml, ie. `wget -O docker-compose.yml https://gitlab.com/geometalab/osmaxx/-/raw/master/docker/docker-compose.prod-test.yml?inline=false` and save it as `docker-compose.yml`.
2. Download the `.env-dist` to the same directory as the docker-compose.yml and save it as .env, ie. `wget -O .env https://gitlab.com/geometalab/osmaxx/-/raw/master/.env-dist`. Fill the variables accordingly, see below for details.
3. Adapt the `docker-compose.yml` to your needs (e.g. change the ports, use something different than caddy for automatic ssl)
4. Run `docker compose up -d`
5. Check the site is working.
6. Create an admin user: `docker compose run frontend bash -c 'poetry run ./manage.py createsuperuser'`

### Create OPENSTREETMAP credentials

With values from the [openstreetmap.org](https://www.openstreetmap.org) (`https://www.openstreetmap.org/user/<user>/oauth_clients/` where user is your username) application settings (OAuth1) using `read their user preferences` as permissions.

The main URL is `<your-url>/` and the callback URL is `https://<your-url>/login/openstreetmap/`.

### Update the project

1. Adapt the `docker-compose.yml` to your needs (e.g. change version of the image pulled)
2. `docker compose pull`
3. `docker compose up -d`

## Development

* [development (with testing and setup instructions)](/docs/development/development.md)

The only supported way running this project is using docker containers.

### Further Documentation

See [development (with testing and setup instructions)](/docs/development/development.md) for more details.
